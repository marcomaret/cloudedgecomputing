#!/bin/sh

if [ "$DATABASE" = "mongodb" ]
then
  echo "Waiting for MongoDB startup..."

  while ! nc -z $MONGO_IP_ADDRESS $MONGO_PORT; do
    sleep 0.1
  done

  echo "MongoDB started"
fi

gunicorn --bind 0.0.0.0:5000 wsgi:app
