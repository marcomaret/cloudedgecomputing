#!/bin/bash

set -e

result=$(curl -w "\n%{http_code}" -X POST http://$ADDRESS:$PORT/api/v1/login -H "Content-Type: application/json" -d '{"username": "admin", "password": "admin"}')

data=${result:0:(-3)}
status_code=${result:(-3)}

if [ $status_code -eq 200 ]; then
  printf "\033[0;32m"
  printf "Test Succeeded - Returned Status Code -> $status_code\n"
  printf "\033[0;36m"

  echo $data | jq -r ".access_token" > access_token
    
  exit 0
else
  printf "\033[0;31m"
  printf "Test Failed - Returned Status Code -> $status_code\n"
  printf "\033[0;36m"
  exit 1
fi
