#!/bin/bash


set -e

TOKEN_PATH="./access_token"

if [ -f "$TOKEN_PATH" ]; then 
  ACCESS_TOKEN=$(cat ./access_token)
  result=$(curl -w "\n%{http_code}" -X GET http://$ADDRESS:$PORT/api/v1/user -H "Content-Type: application/json" -H "Authorization: Bearer ${ACCESS_TOKEN}")

  status_code=${result:(-3)}
  if [ $status_code -eq 200 ]; then
    printf "\033[0;32m"
    printf "Test Succeeded - Returned Status Code -> $status_code\n"
    printf "\033[0;36m"
    exit 0
  else
    printf "\033[0;31m"
    printf "Test Failed - Returned Status Code -> $status_code\n"
    printf "\033[0;36m"
    exit 1
  fi
else
  printf "\033[0;33m"
  printf "ACCESS_TOKEN Not found, make sure you have it saved in file named: $TOKEN_PATH"
  printf "\033[0;36m"
fi

exit 0

