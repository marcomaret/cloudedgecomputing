#!/bin/bash

printf "***********START CURL TESTING***********\n"

counter=0
while true; do
  if nc -z $ADDRESS $PORT; then
    
    printf "\033[0;33m"
    printf "API Available on port $PORT\n"
    printf "\033[0;36m"

    for file in $(ls $1);
    do 
      printf "************RUN $file TEST***************\n"

      start=$(date +%s.%N)

      # RUN TEST
      ./$1/$file

      duration=$(echo "$(date +%s.%N) - $start" | bc)

      execution_time=`printf "%.2f seconds" $duration`
      printf "Test Execution Time: $execution_time\n\n"
    done

    rm -f ./access_token

    exit 0
  fi

  if [ $counter -gt 1000 ]; then
    printf "\033[0;31m"
    printf "ERROR: Cannot access API. Please make sure that container is up and running\n"
    printf "\033[0;36m"
    exit 1
  else
    printf "\033[0;37m"
    printf "Waiting for API.."
    printf "\033[0;36m"
    sleep 0.1
    counter=$((counter+1))
  fi
done
