FROM python:3.10-slim

RUN apt-get update && apt-get install -y netcat curl

RUN mkdir -p /home/app

# Create group and user 'app'
RUN addgroup --system app && adduser --system app && usermod -a -G app app

ENV HOME=/home/app
ENV APP_HOME=/home/app/flask-spacy

RUN mkdir APP_HOME

WORKDIR $APP_HOME

# Install dependencies
COPY ./requirements.txt .
RUN pip install --upgrade pip && pip install -r requirements.txt && pip install gunicorn

RUN pip install -U spacy
RUN python3 -m spacy download en_core_web_sm

COPY ./entrypoint.sh $APP_HOME
COPY ./entrypoint.dev.sh $APP_HOME
COPY ./app $APP_HOME
RUN chmod +x $APP_HOME/entrypoint.sh

RUN chown -R app:app $APP_HOME

USER app

ENTRYPOINT ["sh","entrypoint.sh"]
