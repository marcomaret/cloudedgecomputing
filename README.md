# DevOp-izing Plagiarism Checker Project
## Introduction
Il progetto di partenza, **Plagiarism Checker**, è una semplice API che ha come scopo principale quello di fornire un servizio per la computazione della *"somiglianza"* tra due fonti testuali. Per il calcolo di questo valore utilizza [Spacy](https://github.com/explosion/spaCy), una libreria *open-source* scritta in Python per **NLP** (Natural Language Processing).

L'obiettivo è trasformare questa repository di partenza secondo gli standard e le best practices utilizzate nel mondo dello sviluppo **DevOps**. In particolare, il focus verrà portato sui meccanismi di automazione, integrazione e, in generale, alle operazioni che rendono più efficiente e rapido lo sviluppo del progetto e la disponibilità del servizio all'utente finale.

## Base Project Structure
Prima di procedere con la trasformazione concreta del progetto, è necessario analizzare e comprendere la base di partenza. L'obiettivo di questa fase è capire quali sono le tecnologie utilizzate dall'API e di quale ecosistema di strumenti ha bisogno per funzionare.

Il progetto base è in realtà molto semplice. La repository contiene sostanzialmente due file: `app.py` e `requirements.txt`. Il primo, definisce l'applicazione in flask vera e propria. Analizzandone attentamente il contenuto si possono notare tre cose importanti:
- Vengono definite 4 routes: `register`, `login`, `profile`, `check_plagiarism`.
- L'applicazione si appoggia su un database `Mongo` per registrare gli utenti che accedono al servizio.
- Due routes (`profile` e `check_plagiarism`) sono accessibili previa autenticazione dell'utente tramite *JWT* (JSON Web Token). 

Il secondo file, `requirements.txt`, definisce semplicemente la lista di pacchetti Python richiesti dall'applicazione per funzionare, installabili tramite `pip`. Oltre a queste dipendenze, è richiesto di scaricare il core model di spaCy che verrà usato dall'applicazione Flask.

Nel file `README.md` vengono inoltre elencati diversi comandi per il **testing** del servizio attraverso richieste via `CURL`. 

## DevOp-izing Steps
<!-- TODO: Migliora i punti, probabilmente aggiungi qualcosa sui docker compose e healtcheck della versione -->
Partendo da queste informazioni, possiamo elencare diversi punti utili alla trasformazione del progetto:
1. Suddividire l'ecosistema in diversi container *Docker*, utili per dividere i diversi aspetti del progetto in microservizi. In particolare:
    - Costruire un'immagine docker per eseguire e servire l'applicazione Flask.
    - Utilizzare un container docker per il database in Mongo.
    - Costruire un'immagine `nginx` adatta a fungere da reverse proxy in ambiente di produzione.
    - Costruire un'immagine separata per eseguire i test. 
2. Nei progetti DevOps uno strumento molto utilizzato sono le **CI/CD Pipeline**. Condividendo la repository del progetto su [GitLab](https://gitlab.com) possiamo definire delle pipeline ed eseguirle gratuitamente tramite i loro *runner*. La pipeline per questo progetto sarà formata dai seguenti passi:
    - Build dell'immagine Flask Spacy 
    - Test dell'immagine tramite uno script che esegue i comandi `CURL` mostrati nel file README del progetto base.
    - Prima di rilasciare l'immagine, controllare che la versione di spacy core sia l'ultima disponibile.
    - Release dell'immagine sulla repository di [DockerHub](https://hub.docker.com/repository/docker/marcomaret/flask-spacy)
    - ~~Eventuale deploy su AWS~~

## Dockerizing
#### flask-spacy
L'applicazione Flask ha diverse dipendenze e proprio per questo conviene costruire un'immagine docker *ad-hoc* che si occupi del download ed installazione di tutti i requisiti. Come immagine di partenza utilizziamo `python:3.10-slim`.

Da qui procediamo sostanzialmente a creare un utente `app`, ad installare le dipendenze python richieste dall'applicazione. Inoltre, installiamo i tools necessari al deployment in production (`gunicorn`). *Vedi file [`./Dockerfile`](./Dockerfile).*

Infatti, utilizziamo la stesso `Dockerfile` per developement e production. Quello che cambierà sarà l'entrypoint utilizzato a tempo di esecuzione nel container. In production, l'applicazione flask-spacy verrà servita tramite gunicorn, mentre negli altri casi tramite il comando `python app.py` in DEBUG mode. *Vedi file [`./entrypoint.sh`](./entrypoint.sh) e [`./entrypoint.dev.sh`](./entrypoint.dev.sh) .*

#### nginx
Per quanto riguarda la produzione, è buona prassi non esporre il servizio direttamente dal container dell'immagine, ma utilizzare un reverse proxy. Un modo semplice e veloce per costruirne uno è tramite `nginx`. 

Per configurare nginx come reverse proxy possiamo definire nel file di configurazione `/etc/nginx/conf.d` come upstream il servizio del container `flask-spacy`, e poi ascoltare sulla porta 80 e passare tutte le richieste direttamente all'upstream. *Vedi file [`./nginx/nginx.conf`](./nginx/nginx.conf)*

#### tests 
Quest'immagine verrà utilizzata per eseguire i test sia in ambiente di produzione che di sviluppo (attraverso la pipeline CI/CD). Come immagine di partenza scegliamo un OS leggero (`Alpine Linux`) e installiamo i tool necessari al testing:
- `curl` per effettuare le richieste all'api.
- `bash` per eseguire gli script.
- `jq` per interpretare i dati in formato JSON nelle risposte dell'api.

Dopodiché, copiamo i file che definiscono i test per ogni routes e l'entrypoint script che esegue tutti i test.
Lo script che esegue un test su una route è molto semplice. Ecco un esempio:
```bash
#!/bin/bash

set -e

result=$(curl -w "\n%{http_code}" -X POST http://$ADDRESS:$PORT/api/v1/register \ 
              -H "Content-Type: application/json" \
              -d '{"username": "admin", "password": "admin"}')

status_code=${result:(-3)} #extract status code

if [ $status_code -eq 201 ]; then
  printf "\033[0;32m"
  printf "Test Succeeded - Returned Status Code -> $status_code\n"
  printf "\033[0;36m"
  exit 0
else
  printf "\033[0;31m"
  printf "Test Failed - Returned Status Code -> $status_code\n"
  printf "\033[0;36m"
  exit 1
fi
``` 
*Vedi file cartella [`./test`](./test)*

## Docker Compose
Tramite lo strumento `docker compose` andiamo a costruire il nostro ambiente eseguendo i vari docker container e utilizzando le immagini che abbiamo appena definito.

Anche in questo caso, per differenziare dall'ambiente di produzione vengono definiti due file distinti: `docker-compose.dev.yml` e `docker-compose.yml`.
Prima di elencare le differenze tra i due file, analizziamo le scelte comuni che sono state fatte per entrambi.

**Network**: i vari container devono poter comunicare tra loro. Per farlo docker permette la costruzione di reti, e tramite il docker compose possiamo assegnare ad ogni servizio varie reti. Per questo progetto, vengono definite due reti:
- `backend`, di cui fanno parte i servizi database e flask-spacy.
- `frontend`, definita solo in production, di cui fanno parte i servizi nginx, flask-spacy e tests.

Queste due reti utilizzano una sottorete con ip statico definito nel file [`.env`](./env). Sempre nel file `.env` sono definiti i vari indirizzi IP per ogni servizio.

#### `docker-compose.dev.yml`
In fase di sviluppo, il docker compose costruisce solamente due servizi:
- `db`, partendo dall'immagine `mongo:latest` e utilizzando le variabili d'ambiente definite in `mongo.env`
- `app`, costruendo l'immagine `flask-spacy` che abbiamo definito nel dockerfile, utilizzando le variabili d'ambiente definite in `app.env`. Questo servizio dipende dal servizo `db`, poiché l'API non può funzionare senza database.

Il servizio `app` viene poi esposto sulla porta (definita nel file `.env`) e quindi accessibile a `localhost:$PORT`. Essendo in fase di developement, non dobbiamo gestire la generazione della chiave `JWT_SECRET_KEY`.

Per avviare il progetto in fase di developement basta eseguire il seguente comando:
```bash
$ docker compose -f docker-compose.dev.yml up -d
[+] Running 3/3
 ⠿ Network cloudedgecomputing_backend  Created
 ⠿ Container cloudedgecomputing-db-1   Started
 ⠿ Container cloudedgecomputing-app-1  Started
```

A questo punto, per eseguire i test in ambiente di sviluppo, possiamo eseguire direttamente lo script di avvio dei test:
```
$ chmod u+x ./test/test.sh
$ ./test/test.sh
***********START CURL TESTING***********
API Available on port 80
************RUN 00_register.sh TEST***************
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100    78  100    36  100    42    550    642 --:--:-- --:--:-- --:--:--  1200
Test Succeeded - Returned Status Code -> 201
Test Execution Time: 0.00 seconds
************RUN 01_login.sh TEST***************
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   331  100   289  100    42   8499   1235 --:--:-- --:--:-- --:--:-- 10030
Test Succeeded - Returned Status Code -> 200
Test Execution Time: 0.00 seconds
************RUN 02_check_plagiarism.sh TEST***************
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   168  100    78  100    90     48     56  0:00:01  0:00:01 --:--:--   105
Test Succeeded - Returned Status Code -> 200
Test Execution Time: 2.00 seconds
```

#### `docker-compose.yml`
In produzione, come anticipato prima, bisogna utilizzare un reverse proxy. Per questo aggiungiamo un servizio nel docker-compose file che crea un container docker a partire da `nginx/dockerfile`. Questo servizio prima di essere creato e lanciato, deve aspettare che il servizio `app` sia attivo e funzionante, poiché non vogliamo che il proxy inizi a passare richieste all'API prima che questa sia pronta. 

In questo caso, aggiungere solamente il campo `depends_on: -app` non basta, perchè questo comando non si assicura che il servizio sia effettivamente raggiungibile. Per aggirare questo problema, creiamo una nuova route nella nostra applicazione chiamata `/production/api/v1/healtcheck`:
```python
@app.route("/production/api/v1/healthcheck", methods=["GET"])
def healthcheck():
    return jsonify({'status':'healthy'}), 200
```
Come si può ben vedere, questa routes non fa altro che restituire uno status code 200. Utilizzeremo infatti questa routes all'interno del docker compose file. In particolare, nel campo `healtcheck` del servizio `app`:
```yaml
services:
  ...
  nginx:
    depends_on:
      app:
        condition: service_healthy
  ...
  app:
    ...
    healthcheck:
      test: ["CMD", "curl", "-f", "0.0.0.0:5000/production/api/v1/healthcheck"]
      interval: 1s
      timeout: 2s
      retries: 100
    ...
  ...
```
In questo modo, il servizio `nginx` prima di partire dovrà aspettare che `app` sia healthy, e questo accadrà solamente quanto il comando `curl -f 0.0.0.0:5000/production/api/v1/healthcheck` avrà successo. In questo modo siamo sicuri che il reverse proxy inizierà a lavorare solamente quando l'app flask-spacy sarà raggiungibile. Questo ci tornerà molto utile anche nell'automazione del testing nella CI/CD pipeline.

Da questa soluzione nasce però un effetto collaterale che potrebbe essere indesiderato: rendere disponibile la route `/production/api/v1/healtcheck` all'utente finale. Per in qualche modo *disabilitare* l'accesso alla route da parte dell'utente finale, possiamo modificare il file di configurazione del reverse proxy:
```
upstream webapp{...}
server {
    listen 80;
    location /production {
      return 500;
    }
    location / {
      proxy_pass http://webapp/;
      ...
    }
}
```
In questo modo, chi tenterà di accedere a una qualsiasi risorsa sotto l'URI `/production` avrà come risposta un internal server error.

Oltre che al servizio `nginx`, nel docker compose di produzione aggiungiamo un servizio `tests`. Questo servizio utilizzerà l'immagine `tests` preparata in precedenza per eseguire tutti i test scripts.

Non vogliamo che questo servizio venga eseguito sempre, ma solamente quando vogliamo lanciare i test. Per questo aggiungiamo un campo `profile: ['tests']` nella definizione all'interno del docker compose file. In docker compose, i profili sono tutti disabilitati di default, quindi il comando `docker compose up` lancierà tutti i servizi tranne `tests`. 

Per eseguire i test invece: `docker compose run --rm tests`.

## CI/CD Pipeline
La pipeline per questo progetto è suddivisa in 3 stage:
1. `Build`
2. `Test`
3. `Release`

Ognuno di questi stage, per poter essere avviato, necessita la corretta esecuzione dello stage precedente. Prima di procedere con l'esecuzione del primo stage, definiamo il campo `before_script` in modo tale da eseguire il comando `docker login` usando le credenziali di DockerHub precedentemente salvate nelle CI/CD variables del progetto.

```bash
docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
```

#### Build Stage
In questo stage della pipeline viene definito un unico job che costruisce l'immagine `flask-spacy` partendo dal dockerfile della repository. Se tutto avviene correttamente, *tagga* l'immagine appena costruita con il codice **SHA** del commit che ha scatenato la pipeline. Infine carica l'immagine su DockerHub.
```yml
build-app:
  stage: build
  script:
    # Build intermediate image of flask-spacy app and push it to registry
    - docker build
      --pull 
      --cache-from "$CI_REGISTRY_IMAGE:latest"
      --tag "$CI_REGISTRY_IMAGE:$CI_COMMIT_SHA"
      .
    - docker push "$CI_REGISTRY_IMAGE:$CI_COMMIT_SHA"
  only:
    - main
```

#### Test Stage
Questo stage è composto da due job: `test-api` and `test-spacy-version`. 

Come si può intuire, il primo esegue il servizio `tests` definito nel docker compose file. Per prima cosa, avvia tramite docker compose tutti i servizi, successivamente costruisce l'immagine del servizio dei test con il comando `docker compose build`, ed infine avvia quest'ultimo per eseguire gli script di testing.

```yml
test-api:
  stage: test
  script:
    - docker compose -f docker-compose.yml up -d
    - docker compose -f docker-compose.yml build tests
    - docker compose -f docker-compose.yml run --rm tests
  only:
    - main
```

Il secondo job utilizza una nuova route di production per controllare che la versione di spacy utilizzata sia la più recente disponibile. La nuova route è definita nel file `app.py` in questo modo:
```python
@app.route("/production/api/v1/version", methods=["GET"])
def version():
    import requests, re, spacy
    url = "https://pypi.org/pypi/spacy/json"
    response = requests.get(url, 
                            headers={"Accept" : "application/json"}).json()
    versions = list(response["releases"].keys())
    # filtering dev versions
    versions = [v for v in versions if re.match("[0-9]\.[0-9]\.[0-9]$", v)] 
    if versions[-1] == spacy.__version__:
        return jsonify({'status':'healthy', 
                        'spacy-version': spacy.__version__, 
                        'spacy-latest': versions[-1]}), 200
    return jsonify({'msg': 'spacy version is not latest'}), 500
```
In breve, effettua una richiesta all'indirizzo [https://pypi.org/pypi/spacy/json](https://pypi.org/pypi/spacy/json), che restituisce tutte le versioni disponibili su pip di spacy. Effettua un parsing del json recuperato e controlla che la versione corrente di spacy sia effettivamente l'ultima disponibile. In quel caso, la risposta avrà status code 200, altrimenti 500 (Internal Server Error).

```yml
test-spacy-version:
  stage: test
  script:
    # If version of spacy is not latest, fail pipeline
    - docker compose -f docker-compose.yml up -d
    - docker compose -f docker-compose.yml 
      exec app 
      curl -f 0.0.0.0:5000/production/api/v1/version ||
      (echo "Please update spacy core version to latest" && exit 1)
  only:
    - main
```
In questo job, similmente a quello precedente, avviamo via docker compose tutti i servizi. La route è mascherata dal reverse proxy, quindi per accedervi dobbiamo direttamente eseguire il comando sul container, senza passare da nginx. Per farlo, possiamo usare il comando `docker compose exec TARGET`, sostituendo target con il servizio dell'app flask-spacy. Il comando che vogliamo eseguire è una richiesta curl alla risorsa `production/api/v1/version`. Nel caso fallisca (restituisce 500 Internal Server Error), facciamo fallire la pipeline con un messaggio di errore che invita ad aggiornare la versione di spacy dell'immagine.

#### Release Stage
Se lo stage dei test esegue senza fallire, la pipeline procede con la fase di release. Per questo progetto, si tratta di caricare l'immagine di `flask-spacy` su DockerHub (eventualmente si potrebbe aggiungere un ulteriore job che esegue il deploy su AWS).

In questo stage, recuperiamo via `docker pull` l'immagine taggata con il codice **SHA** del commit che è stata precedentemente caricata nello stage di build. Ora che sappiamo che quest'immagine ha superato i test, possiamo taggarla come `latest` e caricarla nuovamente su DockerHub.

```yml
release-app:
  stage: release
  script:
    # Pull intermediate image, retag as latest, then release to registry
    - docker pull $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
    - docker tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA $CI_REGISTRY_IMAGE:latest
    - docker push $CI_REGISTRY_IMAGE:latest
  only:
    - main
```

## Experiments

#### Testing CI/CD Pipeline
Come prova del funzionamento della pipeline, possiamo analizzare l'esecuzione dei vari stage in seguito di determinati commit. Ad esempio, [qui](https://gitlab.com/marcomaret/cloudedgecomputing/-/commit/86989b1732347c4f8901bc14f1bb8842159354a7) possiamo notare il fallimento della pipeline in seguito al commit che aggiunge, nello stage di test, il controllo della versione di spacy. A fallire è proprio il nuovo job `test-spacy-version` appena aggiunto.

Il [commit successivo](https://gitlab.com/marcomaret/cloudedgecomputing/-/commit/ae23fade9d91a51cd0e1427650cdb5a80718ca7c) aggiunge una semplice riga nel Dockerfile che crea l'immagine di flask-spacy:
```diff
...
COPY ./requirements.txt .
RUN pip install --upgrade pip && pip install -r requirements.txt && pip install gunicorn
- # To improve
+ RUN pip install -U spacy
RUN python3 -m spacy download en_core_web_sm
COPY ./entrypoint.sh $APP_HOME
...
```
In questo modo ci assicuriamo che la versione di spacy si sempre aggiornata. A conferma di questo, possiamo notare come la pipeline scatenata da quest'ultimo commit esegua tutti gli stage correttamente.

#### Adding new API test
Proviamo ad aggiungere un nuovo script di testing e a testare il suo funzionamento all'interno della pipeline. Per prima cosa, creiamo un nuovo file all'interno della cartella `test/routes` chiamato
`03_user.sh`. L'obiettivo di questo test è chiamare la route `/api/v1/user` utilizzando l'access\_token generato dai test prima e assicurarsi che l'API risponda correttamente. *Vedi file [`tests/routes/03_user.sh`](tests/routes/03_user.sh)*

Testiamo il funzionamento del nuovo test (e di tutti quelli pre-esistenti) in locale:
```bash
$ docker compose up -d
$ docker compose build tests
$ docker compose run --rm tests
[+] Running 3/0
⠿ Container cloudedgecomputing-db-1     Running
⠿ Container cloudedgecomputing-app-1    Running
⠿ Container cloudedgecomputing-nginx-1  Running
[+] Running 1/1
⠿ Container cloudedgecomputing-app-1  Healthy

***********START CURL TESTING***********
API Available on port 80
************RUN 00_register.sh TEST***************
% Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                               Dload  Upload   Total   Spent    Left  Speed
100    76  100    34  100    42  16152  19952 --:--:-- --:--:-- --:--:-- 76000
Test Succeeded - Returned Status Code -> 201
Test Execution Time: 0.30 seconds

************RUN 01_login.sh TEST***************
% Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                               Dload  Upload   Total   Spent    Left  Speed
100   331  100   289  100    42   126k  18758 --:--:-- --:--:-- --:--:--  161k
Test Succeeded - Returned Status Code -> 200
Test Execution Time: 0.03 seconds

************RUN 02_check_plagiarism.sh TEST***************
% Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                               Dload  Upload   Total   Spent    Left  Speed
100   168  100    78  100    90    129    149 --:--:-- --:--:-- --:--:--   278
Test Succeeded - Returned Status Code -> 200
Test Execution Time: 1.00 seconds

************RUN 03_user.sh TEST***************
% Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                               Dload  Upload   Total   Spent    Left  Speed
100    33  100    33    0     0  11446      0 --:--:-- --:--:-- --:--:-- 16500
Test Succeeded - Returned Status Code -> 200
```

Ora che abbiamo testato il corretto funzionamento del nuovo test, possiamo effettuare il push dei cambiamenti nel branch main della repository. Il risultato della pipeline 
è visibile [qui](https://gitlab.com/marcomaret/cloudedgecomputing/-/commit/2563488a52284e320c1351673fd1f1e3066bd5fa).

## Limits of Project Choises and Possible Improvements 
#### Network Static IP Addressing
All'interno del docker compose i network vengono costruiti assegnando ad ogni servizio un indirizzo IP statico, elencati all'interno dell'env file. Questa scelta è stata fatta a causa dell'implementazione della connessione con il database nel progetto Flask. 

Infatti, essa avveniva aspettandosi di trovare il mongodb all'indirizzo `172.17.0.2`, ma questo può non essere sempre vero (ad es. è già presente un container in esecuzione con quell'ip).

Di conseguenza ho cambiato l'implementazione dell'app Flask in modo tale da recuperare l'IP del servizio `db` dallo stesso env file che utilizza il docker compose per assegnare gli indirizzi nei network.

Una scelta diversa sarebbe potuta essere quella di integrare il servizio che fornisce il database direttamente all'interno dell'immagine flask-spacy. Questo però avrebbe vincolato l'immagine all'utilizzo di mongodb come database, eliminando in futuro la possibilità di implementare connessioni con altri tipo di db.

#### Handling JWT_SECRET_KEY
Per ora, la pipeline non preve il deploying finale del progetto su un server. Qualora si volesse aggiungere questa feature, sarebbe prima necessario implementare un modo differente per gestire il JWT_SECRET_KEY utilizzato dall'applicazione per generare i token di accesso.

Un possibile modo sarebbe quello di differenziare i file env per la produzione e, in questi, generare in modo randomico la chiave segreta che poi verrà utilizzata dal server.

#### Pushing Intermediate Images in Public Repository
Nello stage `build` della pipeline costruiamo un'immagine intermedia dell'app flask-spacy e la carichiamo su DockerHub. Questo di per sé può essere un problema, perchè il caricamento avviene prima dello stage `test`, quindi nella repository di DockerHub si potrebbero trovare immagini intermedie non funzionanti. 

Questo problema è facilmente aggirabile modificando il push dell'immagine intermedia, effettuandolo non più sulla repository pubblica, ma su una **repository privata** (magari anche non su DockerHub, ma su un server privato).

In questo modo, nello stage `release` possiamo fare il `pull` dell'immagine intermedia dalla repo privata, taggarla come latest e poi fare il push nella repository pubblica.  

<!-- TODO: -->
<!-- - Generate random JWT secret in production environment -->
<!-- - Diff env file for prod? -->
<!-- - Add volumes -->
<!-- Parlare delle scelte fatte: come ip statici -->
