# Plagiarism Checker

## Project description

This project is a basic api developed in flask that checks for plagiarism, it is based upon an open-source 
NPL library: https://github.com/explosion/spaCy

## Project configuration

In order to make it work the requisite are
```
mongo
python
flask
spacy
```

### Configure python environment

(optional) Configure a virtualenv before by installing `python-virtualenv` and executing the command:
```shell
virtualenv venv && source venv/bin/activate
```

To configure python environment simply launch
```shell
pip install -r requirements.txt
```
NOTE: if you get an error installing mysqlclient, try installing python-mysqlclient in the system, it will install required packages

### Install and configure database
In order to interact with a mysql db, install mysql and create a db (edit configuration in `django_project/prod-settings.py` accordingly). For local development sqlite will be used.


## First Start in Development
Run a mongo instance, to run it with mongo use the following command:
```shell
docker run --rm -e MONGO_INITDB_ROOT_USERNAME=admin -e MONGO_INITDB_ROOT_PASSWORD=admin -e MONGO_INITDB_DATABASE=fastapi mongo
```
mongodb will be reachable to its ip, usually `172.17.0.2`, to be sure you can execute:
```shell
docker inspect mongodb-name-or-id -f '{{json .NetworkSettings.Networks }}'    
```

then download spacy model packages
```shell
python -m spacy download en_core_web_sm
```
finally run the application
```shell
python app.py
```

### cURL test
Fundamental cURL test url, these could be useful to test that everything is working correctly

#### register a new user
```shell
curl -X POST http://127.0.0.1:5000/api/v1/register -H "Content-Type: application/json" -d '{"username": "admin", "password": "admin"}'
```
#### login with a user
```shell
curl -X POST http://127.0.0.1:5000/api/v1/login -H "Content-Type: application/json" -d '{"username": "admin", "password": "admin"}' 
```
save the access-token

#### check for similarity between texts
```shell
curl -X POST http://127.0.0.1:5000/api/v1/check -H "Content-Type: application/json" -H "Authorization: Bearer ${ACCESS_TOKEN}" -d '{"text1":"I would like to explain my issue here", "text2":"I want to show my issue there"}'

```
previous access token into `${ACCESS_TOKEN}` 


## PRODUCTION 


In order to deploy the project in production, you have to decide a strategy following flask suggestion at: https://flask.palletsprojects.com/en/2.2.x/deploying/#self-hosted-options

Remember to run an instance of MongoDB

as in development case you should download spacy core:
```shell
python -m spacy download en_core_web_sm
```

## DEVOPS assignment

For the DevOps improvements of this project there is no need for change to the logic of the application; however 
integration in order to expose call needed for DevOps purpose are allowed. In example, it would be useful to insert a call
to verify that the spacy core version.

As usual, DevOps changes must concentrate on automation and enhancement of the application's maintainer experience, as shown during last seminar on the matter.

### examples

WRONG enhancement:

Integrate directly in the main app a route to verify spacy core version. This kind of feature should not be available 
to platform users. You may think "but it should be, so the user knows which version the platform is using",
yet the answer is no, for DevOps purpose you should make so that it is **always** at the latest version.


RIGHT enhancement:

Create a route available only in build phase that verify if spacy core is at its latest version, and if not it updates it.
