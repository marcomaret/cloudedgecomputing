import hashlib
import datetime
import os
from flask import Flask, request, jsonify
from flask_jwt_extended import JWTManager, create_access_token, jwt_required, get_jwt_identity
from pymongo import MongoClient


app = Flask(__name__)
jwt = JWTManager(app)

app.config['JWT_SECRET_KEY'] = os.environ['JWT_SECRET_KEY']
app.config['JWT_ACCESS_TOKEN_EXPIRES'] = datetime.timedelta(days=1)

client = MongoClient(os.environ.get('MONGO_IP_ADDRESS', '172.17.0.2'), int(os.environ.get('MONGO_PORT', 27017)), username='admin', password='admin')
db = client['plagiarism-checker']
users = db["users"]

@app.route("/production/api/v1/healthcheck", methods=["GET"])
def healthcheck():
    return jsonify({'status':'healthy'}), 200

@app.route("/production/api/v1/version", methods=["GET"])
def version():
    import requests, re, spacy
    url = "https://pypi.org/pypi/spacy/json"
    response = requests.get(url, 
                            headers={"Accept" : "application/json"}).json()
    versions = list(response["releases"].keys())
    versions = [v for v in versions if re.match("[0-9]\.[0-9]\.[0-9]$", v)] # filtering dev versions
    if versions[-1] == spacy.__version__:
        return jsonify({'status':'healthy', 'spacy-version': spacy.__version__, 'spacy-latest': versions[-1]}), 200
    return jsonify({'msg': 'spacy version is not latest'}), 500


@app.route("/api/v1/register", methods=["POST"])
def register():
    new_user = request.get_json()
    new_user["password"] = hashlib.sha256(new_user["password"].encode("utf-8")).hexdigest()
    doc = users.find_one({"username": new_user["username"]})
    if not doc:
        users.insert_one(new_user)
        return jsonify({'msg': 'User created successfully'}), 201
    return jsonify({'msg': 'Username already exists'}), 409


@app.route("/api/v1/login", methods=["POST"])
def login():
    login_details = request.get_json()
    user_from_db = users.find_one({'username': login_details['username']})

    if user_from_db:
        encrpted_password = hashlib.sha256(login_details['password'].encode("utf-8")).hexdigest()
        if encrpted_password == user_from_db['password']:
            access_token = create_access_token(identity=user_from_db['username'])
            return jsonify(access_token=access_token), 200

    return jsonify({'msg': 'The username or password is incorrect'}), 401


@app.route("/api/v1/user", methods=["GET"])
@jwt_required()
def profile():
    current_user = get_jwt_identity()
    user_from_db = users.find_one({'username': current_user})
    if user_from_db:
        del user_from_db['_id'], user_from_db['password']
        return jsonify({'profile': user_from_db}), 200
    return jsonify({'msg': 'Profile not found'}), 404


@app.route("/api/v1/check", methods=["POST"])
@jwt_required()
def check_plagiarism():
    user_request = request.get_json()
    current_user = get_jwt_identity()
    user_from_db = users.find_one({'username': current_user})
    if not user_from_db:
        return jsonify({'msg': 'Profile not found'}), 404
    del user_from_db['_id'], user_from_db['password']
    try:
        import spacy
    except ImportError:
        return jsonify({'msg': 'Module spacy needed to exec esteem'}), 500
    nlp = spacy.load('en_core_web_sm')
    text1 = nlp(user_request['text1'])
    text2 = nlp(user_request['text2'])
    ratio = text1.similarity(text2)
    return jsonify({
        'ratio': ratio,
        'msg': 'Similarity score calculated successfully'
    }), 200
