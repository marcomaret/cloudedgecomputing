#!/bin/sh

if [ "$DATABASE" = "mongodb" ]
then
  echo "Waiting for MongoDB startup..."

  while ! nc -z $MONGO_IP_ADDRESS $MONGO_PORT; do
    sleep 0.1
  done

  echo "MongoDB started"
fi

flask run --host=0.0.0.0
